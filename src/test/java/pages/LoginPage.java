package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import net.thucydides.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginPage extends PageObject {

    public LoginPage(AppiumDriver<WebElement> driver) {
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }
    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"txtPhone\"]")
    public WebElement txtPhone;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"txtPassword\"]")
    public WebElement txtPassword;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnLogin\"]")
    public WebElement btnLogin;


    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"txtContentAlert\"]" )
    public WebElement txtContentAlert;

    public void inputPhone(String phone){
        txtPhone.sendKeys(phone);
    }
    public void inputPassword(String password){
        txtPassword.sendKeys(password);
    }
    public void clickLogin(){
        btnLogin.click();

    }

    public void verifyAlert(String message){
        Assert.assertEquals(message,txtContentAlert.getText());
    }
}
