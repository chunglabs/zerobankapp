
Feature: Login
  As a user
  I want to buy some ZB
  So that I need to login Zero Bank App

  Scenario Outline: Login successfully
    Given The user opened Zerobank Application
    When He choose "<country>"
    And He fill "<phone>" and "<password>" to login
    Then He should see home page

    Examples:
      | country | phone | password |
      | Vietnam     | 943313848     | Minh2018@ |