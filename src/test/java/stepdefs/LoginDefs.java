package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.LoginSteps;

public class LoginDefs {
    @Steps
    LoginSteps loginSteps;
    @Given("^The user opened Zerobank Application$")
    public void theUserOpenedZerobankApplication() {
        System.out.println("");
    }

    @When("^He choose \"([^\"]*)\"$")
    public void he_choose_country(String country) throws Throwable {
        System.out.println(country);
    }

    @And("^He fill \"([^\"]*)\" and \"([^\"]*)\" to login$")
    public void he_fill_phone_and_password(String phone, String password) throws Throwable {
        loginSteps.login(phone, password);
    }

    @Then("^He should see home page$")
    public void heShouldSeeHomePage() {

    }
}
