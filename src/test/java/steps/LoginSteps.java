package steps;

import io.appium.java_client.AppiumDriver;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import pages.LoginPage;

public class LoginSteps {
    LoginPage loginPage;
    @Step
    public void login(String phone, String password){
        loginPage.inputPhone(phone);
        loginPage.inputPassword(password);
        loginPage.clickLogin();
    }
}
